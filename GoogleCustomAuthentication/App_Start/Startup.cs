﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin;
using System.Web.Security;

[assembly: OwinStartup(typeof(GoogleCustomAuthentication.App_Start.Startup))]
namespace GoogleCustomAuthentication.App_Start
{
    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
        public void ConfigureAuth(IAppBuilder app)
        {
            // + Set Default Authentication
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            // + Use Cookies 
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/Account/Index")
            });
            // +  Use Google Authentication
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "366512963446-362ba6s05dlt4938sv0he7gtbhnkj51h.apps.googleusercontent.com",
                ClientSecret = "Dp9hp_mVAQmd59Szv5KQ_ip6",
                //CallbackPath = new PathString("/GoogleLoginCallback")
            });
        }
    }
}