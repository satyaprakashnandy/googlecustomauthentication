﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoogleCustomAuthentication.Models;

namespace GoogleCustomAuthentication.Dal
{
    public class GoogleCustomAuthenticationContext : DbContext
    {
        public GoogleCustomAuthenticationContext() : base()
        {

        }
        public DbSet<User> User { get; set; }
    }
}