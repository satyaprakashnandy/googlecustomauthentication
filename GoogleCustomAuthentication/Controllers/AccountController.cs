﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using GoogleCustomAuthentication.Models;
using System.Security.Claims;
using GoogleCustomAuthentication.Services;
using GoogleCustomAuthentication.Dal;

namespace GoogleCustomAuthentication.Controllers
{
    public class AccountController : Controller
    {
        private GoogleCustomAuthenticationContext context = new GoogleCustomAuthenticationContext();
        GoogleUserImplementation googleUserService;
        ClaimImplementation claimService;
        public AccountController(GoogleUserImplementation googleUserService,ClaimImplementation claimService)
        {
            this.googleUserService = googleUserService;
            this.claimService = claimService;
        }
        // GET: Account
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public void GoogleSignIn()
        {
            if(!Request.IsAuthenticated)
            {
                HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = "Account/GoogleLoginCallback"}, "Google");
            }
        }
        public ActionResult GoogleLoginCallback()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                IdentityModel identity = googleUserService.GetUserIdentity(HttpContext);
                googleUserService.RegisterUser(identity);
                HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties { IsPersistent = false }, claimService.GetClaim(identity));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Index(LoginViewModel loginViewModel)
        {
            if (context.User.Any(u => u.Email == loginViewModel.Email && u.Password == loginViewModel.Password))
            {
                HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties { IsPersistent = false},claimService.GetClaim(new IdentityModel { Emailaddress = loginViewModel.Email }));
                return RedirectToAction("Index","Home");
            }
            else
            {
                ViewBag.status = "Invalid Email Or Password";
                return View();
            }
        }
    }
}