﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleCustomAuthentication.Models
{
    public class IdentityModel
    {
        public string Emailaddress { get; set; }
        public string Name { get; set; }
        public string GivenName { get; set; }
        public string SurName { get; set; }
        public string NameIdentifier { get; set; }
    }
}