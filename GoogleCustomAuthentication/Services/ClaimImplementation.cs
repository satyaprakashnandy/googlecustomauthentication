﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using GoogleCustomAuthentication.Models;
using Microsoft.Owin.Security.Cookies;

namespace GoogleCustomAuthentication.Services
{
    public class ClaimImplementation : IClaim
    {
        public ClaimsIdentity GetClaim(IdentityModel identity)
        {
            List<Claim> claimList = new List<Claim>();
            claimList.Add(new Claim(ClaimTypes.Email,identity.Emailaddress));
            claimList.Add(new Claim(ClaimTypes.Name, identity.Name==null ? "Registered User" : identity.Name));
            ClaimsIdentity claim = new ClaimsIdentity(claimList,CookieAuthenticationDefaults.AuthenticationType);
            return claim;
        }
    }
}