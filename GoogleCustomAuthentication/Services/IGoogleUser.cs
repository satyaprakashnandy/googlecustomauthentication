﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using GoogleCustomAuthentication.Models;

namespace GoogleCustomAuthentication.Services
{
    public interface IGoogleUser
    {
        void SignIn(IdentityModel identity);
        IdentityModel GetUserIdentity(HttpContextBase context);

        int RegisterUser(IdentityModel identity);
    }
}