﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using GoogleCustomAuthentication.Models;

namespace GoogleCustomAuthentication.Services
{
    interface IClaim
    {
        ClaimsIdentity GetClaim(IdentityModel identity);
    }
}
