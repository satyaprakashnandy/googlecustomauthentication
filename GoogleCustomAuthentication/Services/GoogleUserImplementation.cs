﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using GoogleCustomAuthentication.Models;
using GoogleCustomAuthentication.Dal;

namespace GoogleCustomAuthentication.Services
{
    public class GoogleUserImplementation : IGoogleUser
    {
       
        
        public IdentityModel GetUserIdentity(HttpContextBase httpContext)
        {
            ClaimsIdentity userIdentity = new ClaimsIdentity(httpContext.User.Identity);
            IdentityModel identity = new IdentityModel
            {
                Emailaddress = userIdentity.Claims.FirstOrDefault(r => r.Type == ClaimTypes.Email).Value,
                Name = userIdentity.Claims.FirstOrDefault(r => r.Type == ClaimTypes.Name).Value,
                GivenName = userIdentity.Claims.FirstOrDefault(r => r.Type == ClaimTypes.GivenName).Value,
                SurName = userIdentity.Claims.FirstOrDefault(r => r.Type == ClaimTypes.Surname).Value
            };
            return identity;
        }

        

        public void SignIn(IdentityModel identity)
        {
           GoogleUserImplementation obj = new GoogleUserImplementation();
        GoogleCustomAuthenticationContext context = new GoogleCustomAuthenticationContext();
            if(!context.User.Any(u => u.Email == identity.Emailaddress))
            {
                if(obj.RegisterUser(identity)<=0)
                {
                    throw new Exception();
                }
            }
        }

        public int RegisterUser(IdentityModel identity)
        {
            GoogleCustomAuthenticationContext context = new GoogleCustomAuthenticationContext();
            context.User.Add(new User{
                FirstName = identity.Name,
                LastName = identity.SurName,
                Email = identity.Emailaddress,
                Details = "I am a new user"
            });
            return context.SaveChanges();
        }

    }
}